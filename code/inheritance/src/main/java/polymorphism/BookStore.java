package polymorphism;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("the dog an Johakim", "Johakim");
        books[1] = new EBook("Every cat has 9 lives, mine has 8", "Oona", 151515);
        books[2] = new Book("Johakim and the dog", "Zyon");
        books[3] = new EBook("Why did the chicken cross", "My dada", 424242);
        books[4] = new EBook("The paper ran out!", "My mom", 77777);

        for (Book book : books) {
            System.out.println(book);
        }
    }
}
