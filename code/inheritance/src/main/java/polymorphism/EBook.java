package polymorphism;

public class EBook extends Book{
    private int numBytes;

    public EBook(String title, String author, int numBytes) {
        super(title, author);
        this.numBytes = numBytes;
    }

    public int getNumBytes() {
        return this.numBytes;
    }

    public String toString() {
        String bookString = super.toString();
        return (bookString + "\nNumber of Bytes : " + this.numBytes);
    }
}

